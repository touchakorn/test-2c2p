CREATE TABLE Transactions (
  id INT NOT NULL PRIMARY KEY,
  date DATETIME2,
  amount FLOAT,
  currency VARCHAR(3),
	status VARCHAR(10),
	customerID INT FOREIGN KEY REFERENCES Customers(customerID)
);