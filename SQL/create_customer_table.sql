CREATE TABLE Customers (
  customerID INT NOT NULL PRIMARY KEY,
  name VARCHAR(30),
  email VARCHAR(25),
  mobile VARCHAR(10),
);