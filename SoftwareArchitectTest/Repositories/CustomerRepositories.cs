﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SoftwareArchitectTest.Models;
using SoftwareArchitectTest.Entities;

namespace SoftwareArchitectTest.Repositories
{
  public class CustomerRepository : IRepository<CustomerEntities>
  {
    private readonly CustomerContext _context;
    public CustomerRepository(CustomerContext context)
    {
      _context = context;
    }

    public CustomerEntities Get(int id, string email)
    {
      return _context.Customers.Where(x => x.customerID == id || x.Email == email)
      .Select(x => new CustomerEntities
      {
        customerID = x.customerID,
        Email = x.Email,
        Name = x.Name,
        Mobile = x.Mobile,
        Transactions = x.Transactions.Where(b => b.customerID == x.customerID).ToList()
      }).FirstOrDefault();
    }

    CustomerEntities IRepository<CustomerEntities>.Get(int id)
    {
      return _context.Customers.Where(x => x.customerID == id)
      .Select(x => new CustomerEntities
      {
        customerID = x.customerID,
        Email = x.Email,
        Name = x.Name,
        Mobile = x.Mobile,
        Transactions = x.Transactions.Where(b => b.customerID == id).ToList()
      }).FirstOrDefault();
    }

    CustomerEntities IRepository<CustomerEntities>.Get(int id, string email)
    {
      throw new NotImplementedException();
    }
  }
}
