﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftwareArchitectTest.Repositories
{
	public interface IRepository<TEntity>
	{
		TEntity Get(int id);
		TEntity Get(int id, string email);
	}
}
