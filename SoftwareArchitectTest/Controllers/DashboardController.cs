﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace SoftwareArchitectTest.Controllers
{
  [Route("api/")]
  [ApiController]
  public class DashboardController : ControllerBase
  {
    [HttpGet]
    public ActionResult<string> Get()
    {
        return "Welcome To MagicBox Test Result";
    }
  }
}
