﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SoftwareArchitectTest.Entities;
using SoftwareArchitectTest.Models;
using SoftwareArchitectTest.Repositories;
using Customer = SoftwareArchitectTest.Entities.CustomerEntities;

namespace SoftwareArchitectTest.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class CustomerController : ControllerBase
  {
    private IRepository<CustomerEntities> _customerRepo;

    // GET api/customer
    [HttpGet]
    public string GetAll(int id)
    {
      return "api ok";
    }

    // GET api/values/5
    [HttpPost]
    public IActionResult Get(SearchCustomer search)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);
        if (search == null) return BadRequest("No inquiry criteria");

        Customer customer;
        customer = _customerRepo.Get(search.customerID, search.email);
        if (customer == null) return NotFound("Not found");

        CustomerModel result = new CustomerModel();
        result.customerID = customer.customerID;
        result.name = customer.Name;
        result.email = customer.Email;
        result.mobile = customer.Mobile;
        result.transactions = customer.Transactions.Select(x => new TransactionModel
        {
          id = x.id,
          date = x.date,
          amount = x.amount,
          currency = x.currency,
          //status = x.status
        }).ToList();

        return Ok(result);
      }
      catch (Exception ex)
      {
        return BadRequest(ex);
      }
    }
  }
}
