﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftwareArchitectTest.Models
{
  public class TransactionModel
  {
    public int id { get; set; }
    public string date { get; set; }
    public double amount { get; set; }
    public string currency { get; set; }
    public int customerID { get; set; }
    public enum status
    {
      Failed = 0,
      Success = 1,
      Canceled = 2
    };
  }
}