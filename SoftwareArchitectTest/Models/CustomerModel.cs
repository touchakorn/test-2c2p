﻿using System.Collections.Generic;

namespace SoftwareArchitectTest.Models
{
    public class CustomerModel
  {
    public int customerID { get; set; }
    public string name { get; set; }
    public string email { get; set; }
    public string mobile { get; set; }
    public List<TransactionModel> transactions { get; set; }
  }
}
