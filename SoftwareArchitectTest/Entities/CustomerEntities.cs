﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SoftwareArchitectTest.Models;

namespace SoftwareArchitectTest.Entities
{
  [Table("Customers")]
	public class CustomerEntities
    {
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int customerID { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string Mobile { get; set; }
		public ICollection<TransactionModel> Transactions { get; set; }
	}
}
