﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftwareArchitectTest.Entities
{
	public class CustomerContext : DbContext
	{
		public CustomerContext(DbContextOptions options)
			: base(options)
		{
		}

		public DbSet<CustomerEntities> Customers { get; set; }
		public DbSet<TransactionEntities> Transactions { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
            modelBuilder.Entity<CustomerEntities>()
            .HasOne<TransactionEntities>(s => (SoftwareArchitectTest.Entities.TransactionEntities)s.Transactions)
			.WithMany(g => (System.Collections.Generic.IEnumerable<SoftwareArchitectTest.Entities.CustomerEntities>)g.Customer)
            .HasForeignKey(s => s.customerID);
		}
	}
}
