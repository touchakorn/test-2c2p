# Software Architect Test

:: this is project for testing
:: for Requirement Document please see in ./document/

- Start Database script in docker
```bash
  docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=mbx12345' -p 1433:1433 -d mcr.microsoft.com/mssql/server:2017-CU8-ubuntu
```

- migrate table
  - create Customers Table
    :: ```./SQL/create_customer_table.sql ```
  - create Transaction Table
    :: ```./SQL/create_transaction_table.sql ```

